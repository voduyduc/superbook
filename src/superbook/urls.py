from django.urls import include, path
from django.contrib import admin
from accounts.views import SignInAndSignUp, LogoutView, AboutView

urlpatterns = [
    path('', SignInAndSignUp.as_view(template_name='home.html'),
        name='home'),
    path('about/', AboutView.as_view(),
        name='about'),
    path('accounts/logout', LogoutView.as_view(),
        name='logout'),

    path('admin/', admin.site.urls),
]
