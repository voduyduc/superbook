from django.urls import include, path
from . import views

urlpatterns = [
    path('', views.SignInAndSignUp.as_view(), name="home", ),
]
